const router = require('express').Router()

const alunoController = require('../controller/alunoController')
const teacherController = require ('../controller/teacherController')
const JSONPlaceholderController = require ('../controller/JSONPlaceholderController')

router.get('/teacher/', teacherController.getTeachers)
router.post ('/cadastroaluno/', alunoController.postAluno)
router.put("/cadastroaluno/", alunoController.updateAluno);
router.delete("/cadastroaluno/:id", alunoController.deleteAluno);


router.get("/external/",JSONPlaceholderController.getUsers);
router.get("/external/io",JSONPlaceholderController.getWebSiteIO);
router.get("/external/com",JSONPlaceholderController.getWebSiteCom);
router.get("/external/net",JSONPlaceholderController.getWebSiteNet);

router.get("/external/filter",JSONPlaceholderController.getCountDomain);


    
module.exports = router