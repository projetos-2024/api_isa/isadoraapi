const axios = require("axios");
module.exports = class JSONPlaceholderController {
  static async getUsers(req, res) {
    try {
      const response = await axios.get(
        "https://jsonplaceholder.typicode.com/users"
      );

      const users = response.data;
      res.status(200).json({
        message:
          "Aqui estão os usuarios captados da API publica JSON Placeholder!",
        users,
      });
    } catch (error) {
      console.log(error);
      res.status(500).json({ error: "Falha ao encontrar usuarios" });
    }
  }

  static async getWebSiteIO(req,res){
    try{
      const response = await axios.get(
        "https://jsonplaceholder.typicode.com/users"
      );
      const users = response.data.filter( 
      (user)=> user.website.endsWith(".io")
      )
      const TESTE = users.length
      res.status(200).json({
        message:
          "Aqui estão os usuarios com dominio IO!",
        users,TESTE
      });

    }catch(error){
      console.log(error);
      res.status(500).json({ error: "Não foi possivel encontrar os usuarios com dominio IO",error });
    }
  }

  static async getWebSiteCom(req,res){
    try{
      const response = await axios.get(
        "https://jsonplaceholder.typicode.com/users"
      );
      const users = response.data.filter( 
      (user)=> user.website.endsWith(".com")
      )
      const TESTE = users.length
      res.status(200).json({
        message:
          "Aqui estão os usuarios com dominio Com!",
        users,TESTE
      });

    }catch(error){
      console.log(error);
      res.status(500).json({ error: "Não foi possivel encontrar os usuarios com dominio Com",error });
    }
  }

  static async getWebSiteNet(req,res){
    try{
      const response = await axios.get(
        "https://jsonplaceholder.typicode.com/users"
      );
      const users = response.data.filter( 
      (user)=> user.website.endsWith(".net")
      )
      const TESTE = users.length
      res.status(200).json({
        message:
          "Aqui estão os usuarios com dominio Net!",
        users,TESTE
      });

    }catch(error){
      console.log(error);
      res.status(500).json({ error: "Não foi possivel encontrar os usuarios com dominio Net",error });
    }
  }
  static async getCountDomain(req, res) {
    const { dominio } = req.query;
  
    if (!dominio) {/*Verifica se o parâmetro foi fornecido*/
      return res.status(400).json({ erro: "Domínio não fornecido" });
    }
  
    try {
      const response = await axios.get(/*Realizar uma requisição HTTP */
        "https://jsonplaceholder.typicode.com/users"
      );
  
      const users = response.data.filter(/*Filtra os usuarios baseado no dominio */
        (user) => user.website.endsWith("." + dominio)
      );
  
      const count = users.length;/*Calcula o numero de usuarios  */
  
      res.status(200).json({
        message: `Aqui estão os usuários com domínio .${dominio}: ${count}!`,
        users,
      });
    } catch (error) {
      console.error(error);
      res
        .status(500)
        .json({
          error: `Não foi possível encontrar os usuários com domínio .${dominio}`,
        });
    }
  }
  
};
