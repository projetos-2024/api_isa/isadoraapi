module.exports = class alunoController {

  static async postAluno(req, res) {
    const { nome, idade, profissão, cursoMatriculado } = req.body;

    console.log(nome+" "+idade+" "+ profissão+" ");
    console.log ("Matriculado no curso" +cursoMatriculado)

    return res.status(201).json({ message: " Aluno Cadastrado" });
  }


  static async updateAluno(req, res) {
    const { nome, idade, profissão, cursoMatriculado} = req.body;
  if(nome !== "" && profissão !== "" && cursoMatriculado!=="" && idade > 0){
    return res.status(200).json({ message:"Aluno Editado" });
  }
  else{
    return res.status(400).json({message:"Existe campos obrigatórios em branco"});
  }
}

static async deleteAluno(req, res) {
const deleteStatus = req.params.id;
if(deleteStatus !== ""){
  return res.status(200).json({ message: 'Aluno excluído com sucesso.' });

}
else{
  return res.status(400).json({ message: 'Aluno não encontrado.' });

}

}

  
};
